# Portfolio // Arbeitsbeispiele von Jörg Sroka #

### German Version ###

### Wofür ist dieses Repository? ###

* Hier finden Sie eine Übersicht zu einigen Projekten, die ich in der Sprache C# programmiert habe. 
* Ich habe bei einigen Projekten für Studenten der BTK Hochschule mitgearbeitet/mitprogrammiert und ansonsten Konsolenanwendungen geschrieben, um die Sprache besser zu beherrschen.
* Außerdem arbeite ich an einem größeren 3D Spiel in Unity welches momentan leider pausiert werden musste.
# Projekte #

### "Adventure of the M" ###
* Zu finden auf dem Branch "Adventure_of_the_M"
* Erstes C# Projekt 
* Ein Konsolenspiel in der Sprache C#, der Spieler ist das "M" und muss versuchen, die Münzen einzusammeln, während der Gegner (E) versucht, diesen zu fangen. 

#### Anleitung zum Starten ####
* Leider habe ich keine Möglichkeit gefunden dies beim Start zu automatisieren.
1. Eingabeaufforderung starten (cmd) 
2. Rechtsklick auf das Fenster (weißer Bereich) 
3. Gehe zu Eigenschaften 
4. Dann zu Schriftart - darunter Rasterschriftart 8x8 auswählen
5. The Adventure of the M.exe starten! 
6. Wenn es nicht funktioniert können Sie mir eine Email über srokajoerg@gmail.com schicken.

### Unbenanntes Spiel ###
* Zu finden auf dem Branch "Unbennantes_Spiel"
* Laufendes Projekt, welches momentan pausiert ist
* Fantasy-Survival-RPG 3D Game in der Unity Engine 

### Tic Tac Toe ###
* Zu finden auf dem Branch "Tic_Tac_Toe"
* Kleines Projekt nebenbei

### Woran habe ich mitgearbeitet? ###

* [No Land // Itch.io](https://hanndragggon.itch.io/no-land) VR SPIEL in Unity 3D
* [The Sounds That Roam at Night // Itch.io](https://hanndragggon.itch.io/thesoundsthatroamatnight) Semesterprojekt von Studenten in Unity 3D

###-----------------------------------------------------------------------------------------------------------------------------------------------------------------###

# Portfolio // Worksamples from Jörg Sroka #

### English Version ###

### For what is this Repository? ###

* Here you can find an overview of my projects which I made in C#.
* I supported students of the BTK University in some Projects with their programming, otherwise I code during my free time to gain more knowledge about Game Development.
* Iam also working on a bigger project which I develop in Unity 3D.
# Projekte #

### "Adventure of the M" ###
* You can find this on the Branch : "Adventure_of_the_M".
* It was my first experience with C# and I learned the language with it.
* Its a small Game made in the Windows cmd. You as the Player "M"(if not changed in options)need to collect Coins "C" while the Enemy "E" is trying to get you.

#### How to start the Game ####
* Sadly i did not found a solution to change the font in the windows cmd so you have to do it manually.
1. Start the Windows cmd.
2. Right click on the window of the cmd.
3. Goto properties.
4. Then you click on font and choose Raster Fonts and change the Font size to 8x8.
5. And now you hopefully can start the Adventure of the M.exe 
6. If its not working you can write an email to srokajoerg@gmail.com.

### Unnamed Game(obviously it is not named yet :D) ###
* You can find this on the Branch "Unbennantes_Spiel"
* Running project currently paused.
* Fantasy-Survival-RPG 3D Game which I develop in the Unity Engine 

### Tic Tac Toe ###
* You can find this on the Branch "Tic_Tac_Toe"
* Small project which i made in my free time

### Projects Iam featured in ###

* [Do you remember... // Itch.io](https://hanndragggon.itch.io/do-you-remember) VR SPIEL in Unity 3D
* [The Sounds That Roam at Night // Itch.io](https://hanndragggon.itch.io/thesoundsthatroamatnight) Semesterproject by Students in Unity 3D